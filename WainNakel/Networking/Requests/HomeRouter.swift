//
//  HomeRouter.swift
//  WainNakel
//
//  Created by Enas Abdallah on 11/25/20.
//

import Foundation
import Alamofire
import ARSLineProgress


class HomeRouter{
    
    static func getNewRandomRestaurant(UID:String ,completion:@escaping (NewRandomRestaurant?, AFError?) -> Void){
        ARSLineProgress.show()
        
        AF.request(ApiRouter.newRandomRestaurantPath(UID: UID), interceptor: CustomInterceptor()).responseJSON {
            (responce:AFDataResponse<Any>)  in
            ARSLineProgress.hide()
            
            switch responce.result{
            
            case .success(let value):
                if let jsonData = value as? [String:Any] , let data = try? JSONSerialization.data(withJSONObject: jsonData, options: .prettyPrinted) {
                    
                    let decoder = JSONDecoder()
                    do {
                        
                        let responceData = try decoder.decode(NewRandomRestaurant.self, from: data)
                        completion(responceData  , nil)
                    }catch{
                        completion(nil , nil)
                        print(error)
                        print(error.localizedDescription)
                    }
                    
                }else{
                    completion(nil , nil)
                }
            case .failure(let error):
                completion(nil , error)
            }
            
        }
        
    }
    
}
