//
//  SetUpImage.swift
//  WainNakel
//
//  Created by Enas Abdallah on 11/28/20.
//

import Foundation
import UIKit
import Kingfisher


struct SetUpImageView {
    static  func setImage(with urlString: String , viewController:UIViewController){
        
        let imageView = UIImageView()
        guard let url = URL.init(string: urlString) else {
            return
        }
        imageView.kf.setImage(with: url)
        imageView.frame = (viewController.parent?.view.frame)!
        ImageViewer.show(imageView, presentingVC: viewController)
    }
}

