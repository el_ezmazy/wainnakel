//
//  URLRequestBuilder.swift
//  WainNakel
//
//  Created by Enas Abdallah on 11/25/20.
//

import Foundation
import Alamofire

protocol URLRequestBuilder: URLRequestConvertible{
    
    var mainURL: URL { get }
    var requestURL: URL { get }
    
    
    // MARK: - Path
    var path: String { get }
    
    // MARK: - Parameters
    var parameters: Parameters? { get }
    
    // MARK: - Methods
    var method: HTTPMethod { get }
    
    var encoding: ParameterEncoding { get }
    
    var urlRequest: URLRequest { get }
}


extension URLRequestBuilder {
  
    var encoding: ParameterEncoding {
           switch method {
           case .put:
               return JSONEncoding.default
           case .get :
                  return URLEncoding.default
           default:
               return JSONEncoding.default
           }
       }
       
    
    var mainURL: URL  {
        return URL(string: "https://wainnakel.com/api/v1/")!
    }
    
    var requestURL: URL {
        return mainURL.appendingPathComponent(path)
    }
    
    
    var header:HTTPHeaders{
        let header = HTTPHeaders()
           print(header)

           return header
       }
       
       
    var urlRequest: URLRequest {
        var request = URLRequest(url: requestURL)
        request.httpMethod = method.rawValue
        print(request)
        return request
    }
    
    
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        return try encoding.encode(urlRequest, with: parameters)
    }
}
