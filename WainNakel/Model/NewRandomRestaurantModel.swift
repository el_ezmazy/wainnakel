//
//  NewRandomRestaurantModel.swift
//  WainNakel
//
//  Created by Enas Abdallah on 11/25/20.
//

import Foundation

// MARK: - NewRandomRestaurant
struct NewRandomRestaurant: Codable ,CodableInit {
    let error, name, id: String
    let link: String
    let cat, catID, rating, lat: String
    let lon, ulat, ulon, newRandomRestaurantOpen: String
    let image: [String]

    enum CodingKeys: String, CodingKey {
        case error, name, id, link, cat
        case catID = "catId"
        case rating, lat, lon
        case ulat = "Ulat"
        case ulon = "Ulon"
        case newRandomRestaurantOpen = "open"
        case image
    }
}
