//
//  ApiRouter.swift
//  WainNakel
//
//  Created by Enas Abdallah on 11/25/20.
//

import Foundation
import Alamofire

enum ApiRouter: URLRequestBuilder {
  
   
    case newRandomRestaurantPath(UID:String)
   
    
    // MARK: - Path
    internal var path: String {
        switch self {
      
        case .newRandomRestaurantPath:
            return "GenerateFS.php"
       
        }
    }
    
    // MARK: - Parameters
    internal var parameters: Parameters? {
        var params = Parameters.init()
        switch self {
     
        case .newRandomRestaurantPath(let uid):
            params["uid"] = uid
       
            
        }
        
        print(params)
        return params
        
    }
    
    // MARK: - Methods
    internal var method: HTTPMethod {
        switch self {
        
        case .newRandomRestaurantPath:
            return .get
        }
    }
    
    
}
