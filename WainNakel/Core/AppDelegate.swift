//
//  AppDelegate.swift
//  WainNakel
//
//  Created by Enas Abdallah on 11/25/20.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import ARSLineProgress

let googleApiKey = "AIzaSyBxZ1-NhnZ2Rd3AinTopbM6EkZmLxNhFDs"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        loadingProgressConfig()

        IQKeyboardManager.shared.enable = true

        GMSPlacesClient.provideAPIKey(googleApiKey)
        //Mark:- integret googleMaps API KEY
        GMSServices.provideAPIKey(googleApiKey)
        
        return true
    }
    
    
    func loadingProgressConfig(){
        ARSLineProgressConfiguration.backgroundViewStyle = .full
        ARSLineProgressConfiguration.backgroundViewColor = UIColor.clear.cgColor
        ARSLineProgressConfiguration.backgroundViewDismissTransformScale = 1
        ARSLineProgressConfiguration.successCircleLineWidth = 30
        ARSLineProgressConfiguration.circleColorInner =  #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        ARSLineProgressConfiguration.circleColorOuter =  #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        ARSLineProgressConfiguration.circleColorMiddle =  #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
        
        
        
    }
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

