//
//  SuggestNewRestaurantViewController.swift
//  WainNakel
//
//  Created by Enas Abdallah on 11/26/20.
//

import UIKit

class SuggestNewRestaurantViewController: UIViewController {

    
    weak var mainViewController : MainViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func suggestBtnPressed(_ sender: Any) {
        let uid = (mainViewController?.currentLatitude ?? "0") + "," + (mainViewController?.currentLangtitude ?? "0")
        mainViewController?.fetchNewSuggestionRestaurant(uid: uid)
    }
    

}
