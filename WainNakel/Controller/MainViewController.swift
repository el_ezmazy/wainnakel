//
//  MainViewController.swift
//  WainNakel
//
//  Created by Enas Abdallah on 11/26/20.
//

import UIKit
import GooglePlaces
import GoogleMaps

class MainViewController: UIViewController {
    
     @IBOutlet weak var mapView: GMSMapView!
     @IBOutlet weak var containerView: UIView!

     var locationManager = CLLocationManager()
     var currentLatitude:String?
     var currentLangtitude:String?
     var userLocation:CLLocation?
  
    weak var homeViewController : HomeViewController?
    weak var menuViewController : MenuViewController?
    weak var suggestNewRestaurantViewController : SuggestNewRestaurantViewController?
  
    
     override func viewDidLoad() {
         super.viewDidLoad()
         // Do any additional setup after loading the view.
         mapView.delegate = self
         initializeTheLocationManager()
     }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showSuggestNewRestaurantViewController()
    }
    
    
         
    func showHomeViewController (restaurantData:NewRandomRestaurant?) {
        
      self.containerView.alpha = 0
        
        if homeViewController == nil {
            homeViewController = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier:"HomeViewController") as? HomeViewController
            homeViewController?.restaurantData = restaurantData

        }
        
        if homeViewController?.parent == nil {
            self.addChild(homeViewController!)
            self.containerView.addSubview(homeViewController!.view)
            homeViewController?.didMove(toParent: self)
            homeViewController?.view.frame.size.height = self.containerView.frame.height
        }
        
        homeViewController?.mainViewController = self
        UIView.animate(withDuration: 0.3, animations: {
            self.containerView.alpha = 1
        }) { (finished) in
            
        }
        
        removeSuggestNewRestaurantViewController()
        removeMenuViewController()
        
    }
    
    
    
    
    func showSuggestNewRestaurantViewController () {
        
        self.containerView.alpha = 0
        
        if suggestNewRestaurantViewController == nil {
            suggestNewRestaurantViewController = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier:"SuggestNewRestaurantViewController") as? SuggestNewRestaurantViewController
        }
        
        
        if suggestNewRestaurantViewController?.parent == nil {
            
            self.addChild(suggestNewRestaurantViewController!)
            self.containerView.addSubview(suggestNewRestaurantViewController!.view)
            suggestNewRestaurantViewController?.didMove(toParent: self)
        }
        suggestNewRestaurantViewController?.mainViewController = self
        UIView.animate(withDuration: 0.3, animations: {
            self.containerView.alpha = 1
        }) { (finished) in

        }
        self.removeHomeViewController()
        
    }
    
    
    func showMenuViewController (restaurantData:NewRandomRestaurant?) {
        
        self.containerView.alpha = 0
        
        if menuViewController == nil {
            menuViewController = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier:"MenuViewController") as? MenuViewController
            menuViewController?.restaurantData = restaurantData

        }
        
        if menuViewController?.parent == nil {
            
            self.addChild(menuViewController!)
            self.containerView.addSubview(menuViewController!.view)
            menuViewController?.didMove(toParent: self)
        }
        menuViewController?.mainViewController = self
        UIView.animate(withDuration: 0.3, animations: {
            self.containerView.alpha = 1
        }) { (finished) in

        }
        self.removeHomeViewController()
    }
    
    
    func removeHomeViewController () {
        
        homeViewController?.willMove(toParent: nil)
        homeViewController?.view.removeFromSuperview()
        homeViewController?.removeFromParent()
        homeViewController = nil
        
    }
    
    func removeSuggestNewRestaurantViewController() {
        
        suggestNewRestaurantViewController?.willMove(toParent: nil)
        suggestNewRestaurantViewController?.view.removeFromSuperview()
        suggestNewRestaurantViewController?.removeFromParent()
        suggestNewRestaurantViewController = nil
        
    }
    
    
    func removeMenuViewController () {
        
        menuViewController?.willMove(toParent: nil)
        menuViewController?.view.removeFromSuperview()
        menuViewController?.removeFromParent()
        menuViewController = nil
        
    }
    
    
    
    
    
    func fetchNewSuggestionRestaurant(uid:String){
        HomeRouter.getNewRandomRestaurant(UID:uid){[weak self](responce , error) in
            
            if let value  = responce{
                
                self?.showHomeViewController(restaurantData: value)

            }else{
                print("error")
                self?.displayMyAlertMessage(userMessage: "something went wrong !!")

            }
        }
       
    }
    
    
    func displayMyAlertMessage(userMessage:String, handler: @escaping (Bool) -> Void = { _ in }) {
        let myAlert = UIAlertController(title:"", message:userMessage, preferredStyle: UIAlertController.Style.alert);
        let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
            handler(true)
        }
        myAlert.addAction(okAction);
        self.present(myAlert, animated:true, completion:nil);
    }
    
 }

 extension MainViewController:CLLocationManagerDelegate,GMSMapViewDelegate {
     
     
     //setUP GoogleMaps
     func initializeTheLocationManager() {
         // User Location
         
         
         locationManager.requestWhenInUseAuthorization()
        

        if locationManager.authorizationStatus() == .authorizedWhenInUse {
             
             locationManager.startUpdatingLocation()
             
         }
      
         locationManager.delegate = self
         locationManager.desiredAccuracy = kCLLocationAccuracyBest
         
     }
     
     
    
     
     func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         userLocation = locations.last
         ShowCurrentLocationMap(userLocation: locations.last!)
        self.currentLatitude = "\(userLocation?.coordinate.latitude ?? 0.0)"
        self.currentLangtitude = "\(userLocation?.coordinate.longitude ?? 0.0)"

         self.locationManager.stopUpdatingLocation()
         
     }
     
     
     
     func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
         print("error:: \(error.localizedDescription)")
     }
     
     
     private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
         if status == .authorizedWhenInUse {
             locationManager.startUpdatingLocation()
             
             mapView.isMyLocationEnabled = true
             mapView.settings.myLocationButton = true
         }
     }
     
     
     func ShowCurrentLocationMap(userLocation:CLLocation){
         let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
         
         let camera = GMSCameraPosition.camera(withLatitude: userLocation.coordinate.latitude,
                                               longitude: userLocation.coordinate.longitude, zoom: 16.0)
         
         CATransaction.begin()
         CATransaction.setValue(1, forKey: kCATransactionAnimationDuration)
         
         DispatchQueue.main.async {
             self.mapView.animate(to: camera)
         }
         CATransaction.commit()
         
         
         self.mapView.clear()
         
         let marker = GMSMarker(position: center)
         
         marker.map = self.mapView
         // I have taken a pin image which is a custom imagelas
         let markerImage = UIImage(named: "LocationMap")!.withRenderingMode(.alwaysTemplate)
         
         //creating a marker view
         let markerView = UIImageView(image: markerImage)
         
         //changing the tint color of the image
         markerView.tintColor = UIColor.purple
                
         marker.iconView = markerView
         
     }
     
 }
