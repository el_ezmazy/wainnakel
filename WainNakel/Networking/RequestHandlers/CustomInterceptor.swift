//
//  CustomInterceptor.swift
//  WainNakel
//
//  Created by Enas Abdallah on 11/25/20.
//

import Foundation
import Alamofire

class CustomInterceptor:RequestInterceptor{
    
  //  private let xApIToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2t3YWZlcmFcL3B1YmxpY1wvYXBpXC9sb2dpbiIsImlhdCI6MTU5MTE1NDYyNCwibmJmIjoxNTkxMTU0NjI0LCJqdGkiOiJGVzAwTEVWbkhKbXo5STlRIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.2bn-goCa8o4WHL2vyjVSd1B-KZum2_eb_cdMnwEMKTg"
 
    
 private let xApIToken = "Bearer "
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        print(xApIToken)

        var urlRequest = urlRequest
        
        if  let urlString = urlRequest.url?.absoluteString , urlString.contains("/api"){
            
            urlRequest.addValue(xApIToken, forHTTPHeaderField: "Authorization")
            print(urlString)
        }
        
        completion(.success(urlRequest))
    }
    
}
