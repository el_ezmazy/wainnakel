//
//  MenuViewController.swift
//  WainNakel
//
//  Created by Enas Abdallah on 11/29/20.
//

import UIKit

class MenuViewController: UIViewController {

    weak var mainViewController : MainViewController?
    var restaurantData:NewRandomRestaurant?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func menuBtnPressed(_ sender: UIButton) {
        mainViewController?.showHomeViewController(restaurantData:restaurantData)
       
    }
   

}
