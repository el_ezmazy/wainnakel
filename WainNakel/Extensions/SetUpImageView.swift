//
//  SetUpImage.swift
//  WainNakel
//
//  Created by Enas Abdallah on 11/28/20.
//

import Foundation
import UIKit
import Kingfisher


import Kingfisher

extension UIImageView {
    func setImage(with urlString: String, handler: @escaping (Bool) -> Void = { _ in }){
        guard let url = URL.init(string: urlString) else {
            return
        }
      //  var kf = self.kf
        kf.indicatorType = .activity
        kf.indicator?.startAnimatingView()
        kf.setImage(
            with:url,
            placeholder: UIImage(named: "logo"),
            options: [.transition(.fade(1)),
                      .cacheOriginalImage
            ])
        {[weak self]
            result in
            guard let self = self else{return}
            self.kf.indicator?.stopAnimatingView()
            switch result {
            
            case .success(let value):
                handler(true)
                self.image = value.image
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
            
        }
        
        
        
        
    }
}

