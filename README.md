WainNakel
=====================



## Requirements
- Swift 5.0
- iOS 14.0

1) Clone the repository

```bash
$ git clone git@gitlab.com:el_ezmazy/wainnakel.git
```

2) Open the workspace in Xcode

```bash
$ cd WainNakel
$ pod install
$ open WainNakel.xcworkspace
```
