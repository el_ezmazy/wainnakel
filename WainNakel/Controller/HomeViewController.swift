//
//  HomeViewController.swift
//  WainNakel
//
//  Created by Enas Abdallah on 11/26/20.
//

import UIKit
import Kingfisher

class HomeViewController: UIViewController {
    
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var restaurantRateLabel: UILabel!
    
    
    weak var mainViewController : MainViewController?
    var restaurantData:NewRandomRestaurant?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateNewRestaurantData(restaurantData: restaurantData)
    }
    
    
    func updateNewRestaurantData(restaurantData:NewRandomRestaurant?){
        guard let restaurantData = restaurantData else{return}
        self.restaurantNameLabel.text = restaurantData.name
        self.restaurantRateLabel.text = "\(restaurantData.rating)/10 - \(restaurantData.cat)"
        
    }
    
    
    @IBAction func anotherSuggestBtnPressed(_ sender: Any) {
        
        let uid = (mainViewController?.currentLatitude ?? "0") + "," + (mainViewController?.currentLangtitude ?? "0")
        HomeRouter.getNewRandomRestaurant(UID:uid){[weak self](responce , error) in
            
            if let value  = responce{
                
                self?.restaurantData = value
                self?.updateNewRestaurantData(restaurantData: value)
                
            }else{
                print("error")
                self?.mainViewController?.displayMyAlertMessage(userMessage: "something went wrong !!")
            }
        }
        
    }
    
    
    @IBAction func menuBtnPressed(_ sender: UIButton) {
        
//        UIView.transition(with: sender, duration: 1.5, options: .transitionCrossDissolve, animations: {
//            sender.setImage(UIImage(named: "SettingGreen"), for: .normal)
//        }, completion: nil)
        self.mainViewController?.showMenuViewController(restaurantData: restaurantData)
        
    }
    
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        mainViewController?.showSuggestNewRestaurantViewController()
        
    }
    
    
    @IBAction func restaurantImageBtnPressed(_ sender: Any) {
        
        DispatchQueue.main.async {
            let imageView = UIImageView()
            imageView.setImage(with: (self.restaurantData?.image[0])!) { (true) in
                imageView.frame = (self.parent?.view.frame)!
                ImageViewer.show(imageView, presentingVC: self)

            }
        }
    }
    
    
    @IBAction func showNewRestaurantLocationBtnPressed(_ sender: Any) {
        let showNewRestaurantLocationViewController = self.storyboard?.instantiateViewController(withIdentifier: "ShowNewRestaurantLocationViewController") as! ShowNewRestaurantLocationViewController
        showNewRestaurantLocationViewController.restaurantLatitude = restaurantData!.lat
        showNewRestaurantLocationViewController.restaurantLongitude = restaurantData!.lon
        showNewRestaurantLocationViewController.modalPresentationStyle = .overFullScreen
        self.present(showNewRestaurantLocationViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func BtnPressed(_ sender: Any) {
        if let resturantUrl = URL(string:restaurantData!.link){
            UIApplication.shared.open(resturantUrl)
            
        }
        
        
    }
    
    
    
}
