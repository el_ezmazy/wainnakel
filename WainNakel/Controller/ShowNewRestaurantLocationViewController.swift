//
//  ShowNewRestaurantLocationViewController.swift
//  WainNakel
//
//  Created by Enas Abdallah on 11/28/20.
//

import UIKit
import GooglePlaces
import GoogleMaps

class ShowNewRestaurantLocationViewController: UIViewController ,GMSMapViewDelegate , CLLocationManagerDelegate{
    
    
    @IBOutlet weak var mapView: GMSMapView!
    
    var restaurantLatitude:String?
    var restaurantLongitude:String?
    var marker = GMSMarker()

    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        initializeTheLocationManager()
        
       
    }
    
    
    func initializeTheLocationManager() {
        mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.isMyLocationEnabled = true
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locationManager.location?.coordinate
        marker.map = nil
        marker = GMSMarker(position: location!)
   
        let currentOrderLocation = CLLocationCoordinate2D(latitude: Double(restaurantLatitude!)!, longitude: Double(restaurantLongitude!)!)
        
        cameraMoveToLocation(toLocation: currentOrderLocation)
       
        self.locationManager.stopUpdatingLocation()
        
    }
    
    func cameraMoveToLocation(toLocation: CLLocationCoordinate2D?) {
                if toLocation != nil {
                    mapView.camera = GMSCameraPosition.camera(withTarget: toLocation!, zoom: 16.0)
                }
        
        self.locationManager.stopUpdatingLocation()
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}


